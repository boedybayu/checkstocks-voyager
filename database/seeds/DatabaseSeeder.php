<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
		$this->call([
			UsersTableSeeder::class,
			CategoriesTableSeeder::class,
			DataRowsTableSeeder::class,
			DataTypesTableSeeder::class,
			MenuItemsTableSeeder::class,
			MenusTableSeeder::class,
			PagesTableSeeder::class,
			PermissionRoleTableSeeder::class,
			PermissionsTableSeeder::class,
			PostsTableSeeder::class,
			ProductsTableSeeder::class,
			RolesTableSeeder::class,
			SettingsTableSeeder::class,
			SuppliersTableSeeder::class,
			TranslationsTableSeeder::class,
			VoyagerDatabaseSeeder::class,
			VoyagerDummyDatabaseSeeder::class,
		]);
    }
}
