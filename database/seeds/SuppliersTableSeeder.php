<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Supplier;

class SuppliersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = new DateTime();

        DB::table('suppliers')->insert([
            ['id' => 18,'name' => 'Missonline', 'created_at' => $date, 'updated_at' => $date],
            ['id' => 19,'name' => 'Noonakusign', 'created_at' => $date, 'updated_at' => $date],
            ['id' => 20,'name' => 'Clothtowear', 'created_at' => $date, 'updated_at' => $date],
            ['id' => 21,'name' => 'Evercloth', 'created_at' => $date, 'updated_at' => $date]
        ]);
    }
}
