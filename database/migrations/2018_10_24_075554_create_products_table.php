<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('supplier_id')->unsigned();
            $table->string('code', 100)->nullable();
            $table->string('supplier_code', 100)->nullable();
            $table->integer('stock')->default(0);
            $table->integer('number_of_purchases')->default(0);
            $table->integer('number_of_sales')->default(0);
            $table->decimal('purchase_price', 10, 2)->default(0);
            $table->decimal('selling_price', 10, 2)->default(0);
            $table->decimal('profit', 10, 2)->default(0);
            $table->string('product_url', 256)->nullable();
            $table->string('supplier_url', 256)->nullable();
            $table->string('note', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
