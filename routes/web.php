<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
 */

//Route::get('supplier-product-data/{id}', 'ScraperController@getSupplierProductData');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

//Clear Cache facade value:
Route::prefix('refresh-config')->group(function () {
    Route::get('all', 'RefreshConfigController@all');
});

