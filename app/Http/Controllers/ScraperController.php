<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\ProductRepository;
use App\Services\ScraperService;

class ScraperController extends Controller
{
    private $scraperService;

    public function __construct(ScraperService $scraperService)
    {
        $this->scraperService = $scraperService;
    }

    /**
     * @deprecated
     */
    public function index(Request $request)
    {
        $process = (new ChromeProcess)->toProcess();
        $process->start();
        $options = (new ChromeOptions)->addArguments(['--disable-gpu', '--headless']);
        $capabilities = DesiredCapabilities::chrome()->setCapability(ChromeOptions::CAPABILITY, $options);
        $driver = retry(5, function () use($capabilities) {
                return RemoteWebDriver::create('http://localhost:9515', $capabilities);
        }, 50);
        $browser = new Browser($driver);
        //$url = "https://shopee.co.id/Real-Polo-Tas-Koper-Hardcase-Fiber-4-Roda-Putar-Size-20-Inch-i.14359764.1414161588";
        $url = ('https://shopee.co.id/evercloth/1110088023?version=b55b67a2a8f0c57bf4fe40a78a35adce');
        $browser->visit($url);
        $browser->waitFor("div.shopee-page-wrapper > div.page-product > div.container > div.product-briefing.flex.card > div.flex.flex-auto");

        // price
        $selector_price = "div.shopee-page-wrapper > div.page-product > div.container > div.product-briefing.flex.card > div.flex.flex-auto div.flex div.flex.items-center > div > div > div";
        $data['price'] = $browser->text($selector_price);

        //$val = $browser->text("#main > div > div.shopee-page-wrapper > div.page-product > div.container > div.product-briefing.flex.card._2cRTS4 > div.flex.flex-auto.k-mj2F > div > div._3DepLY > div > div.flex._2MBMHS._3a2wD- > div > div.flex.items-center._1FzU2Y > div.flex.items-center > div:nth-child(2)");

        $browser->quit();
        $process->stop();

        return $data;

    }

    public function getSupplierProductData(Request $request, $id)
    {
        return $this->scraperService->getSupplierProductData($id);
    }

}
