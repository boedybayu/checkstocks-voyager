<?php
namespace App\Services;

use App\Repositories\ProductRepository;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Goutte;
use Laravel\Dusk\Browser;
use Laravel\Dusk\Chrome\ChromeProcess;

class ScraperService
{
    private $productRepository;

    private $hostMaps = [
        'shopee.co.id' => 'getProductDataFromShopeeCoId',
        'missonlineshop.com' => 'getProductDataFromMissonlineshopCom',
    ];

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @return ChromeProcess
     */
    private function createProcess()
    {
        return (new ChromeProcess)->toProcess();
    }


    /**
     * @return Laravel\Dusk\Browser;
     */
    private function createBrowser()
    {
        $options = (new ChromeOptions)->addArguments([
            '--disable-gpu',
            '--headless',
            '--no-sandbox',
            '--ignore-ssl-errors',
            '--whitelisted-ips=""',
        ]);

        $capabilities = DesiredCapabilities::chrome()->setCapability(
            ChromeOptions::CAPABILITY,
            $options
        );

        $webDriverUrl = config('app.web_driver_url');
        $webDriverPort = config('app.web_driver_port');

        $driver = retry(5, function () use(
            $capabilities,
            $webDriverPort,
            $webDriverUrl
        ) {
            return RemoteWebDriver::create(
                $webDriverUrl.':'.$webDriverPort,
                $capabilities
            );
        }, 50);

        return new Browser($driver);
    }


    /**
     * Return array of productData from supplier's product page url
     *
     * @param int $id
     *
     * @return array
     */
    public function getSupplierProductData($id)
    {

        // get Product
        $product = $this->productRepository->show($id);

        // get productData base on supplier_url
        $productData = $this->getProductDataBaseOnUrl($product->supplier_url);

        return $productData;

    }

    /**
     * Return array of product data base on Url that is match with $hostMaps
     * or defined url that can be crawl/scrap
     *
     * @param string $url
     *
     * @return array
     */
    private function getProductDataBaseOnUrl($url)
    {
        $parsedUrl = parse_url($url);

        $host = $parsedUrl['host'];

        $productData = [];

        //try {

            foreach ($this->hostMaps as $key => $hostMap) {

                if (strpos($host, $key) !== false) {

                    $productData = $this->$hostMap($url);
                    break;

                }

            }

        //} catch (\Exception $e) {

            // todo catch the Exception

        //}

        return $productData;

    }

    /**
     * Get product data from shopee.co.id
     * use Chromedriver
     *
     * @param string $url from shopee.co.id
     *
     * @return collection
     */
    private function getProductDataFromShopeeCoId($url)
    {
        $resource = collect();
        $data = collect();

        $process = $this->createProcess();
        $process->start();

        // create browser
        $browser = $this->createBrowser();

        // selectors
        $selectors = [
            'price' => "div.shopee-page-wrapper > div.page-product > div.container > div.product-briefing.flex.card > div.flex.flex-auto div.flex div.flex.items-center > div > div > div",
            'stock' => "div.page-product > div.container > div.product-briefing.flex.card > div.flex.flex-auto > div > div:nth-child(4) > div.flex > div:nth-last-child(1) > div.flex-column > div.flex:nth-last-child(1) > div:nth-last-child(1) > div:nth-last-child(1)",
        ];

        $browser->visit($url);

        // wait until element is loaded
        $browser->waitFor("div.shopee-page-wrapper > div.page-product > div.container > div.product-briefing.flex.card > div.flex.flex-auto", 3);

        $variations = $browser->elements('button.product-variation');

        if (!empty($variations)) {

            foreach ($browser->elements('button.product-variation') as $variation) {

                $product = collect();

                $variation->click();

                // variantion name
                $product->put('variation', $variation->getText());

                // price
                $product->put('price', $browser->text($selectors['price']) ?? null);

                // stock
                $product->put('stock', $browser->text($selectors['stock']) ?? null);

                if (!empty($product)) {
                    $data->push($product);
                }
            }

        } else {

            $product = collect();

            // price
            $product->put('price', $browser->text($selectors['price']) ?? null);

            // stock
            $product->put('stock', $browser->text($selectors['stock']) ?? null);

            if (!empty($product)) {
                $data->push($product);
            }

        }

        $browser->quit();
        $process->stop();

        $resource->put('data', $data);
        $resource->put('meta', []);

        return $resource;
    }

    /**
     * Get product data from missonlineshop.com
     * use Goutte
     *
     * @param string $url product page from missonlineshop.com
     *
     * @return collection
     */
    private function getProductDataFromMissonlineshopCom($url)
    {
        $resource = collect();
        $data = collect();
        $meta = collect();

        // selectors
        $selectors = [
            'price' => '.price > label',
        ];

        $crawler = Goutte::request('GET', $url);

        // set globalPrice
        $globalPrice = null;

        $crawler->filter('.price > label')->each(function ($node) use (&$globalPrice) {
            $globalPrice = $node->text();
        });

        // get variations
        $crawler->filter('select.form-control option')->each(function ($node) use (&$data, $globalPrice) {

            $product = collect();

            $variationRaw = trim($node->text());

            $position = strpos($variationRaw, '[');

            // variantion name
            $product->put('variation', trim(substr($variationRaw, 0, $position)));

            // price
            $product->put('price', $globalPrice);

            // stock
            $product->put('stock', substr($variationRaw, $position));

            if (!empty($product)) {
                $data->push($product);
            }

        });

        $resource->put('data', $data);
        $resource->put('meta', $meta);

        return $resource;
    }

}
