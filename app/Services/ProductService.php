<?php
namespace App\Services;

use App\Repositories\ProductRepository;
use ScraperService;

class ProductService
{
    private $scraperService;

    public function __construct(ScraperService $scraperService)
    {
        $this->scraperService = $scraperService;
    }

    /**
     *
     * @param int $id product_id
     *
     * @return JSON
     */
    public function getSupplierProductDetail($id)
    {
        $product = Product::whereId($id)->first();

        $parsedUrl = parse_url($product->supplier_url, PHP_URL_HOST);

        $resource = $this->scraperService->getProductDataBaseOnUrl($url)

        return $resource->toJson();
    }

}
