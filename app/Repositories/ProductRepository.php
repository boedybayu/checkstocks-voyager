<?php
namespace App\Repositories;

use App\Product;

class ProductRepository implements RepositoryInterface
{
    use RepositoryTrait;

    public function __construct(Product $model)
    {
        $this->model = $model;
    }

}
