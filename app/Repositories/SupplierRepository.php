<?php
namespace App\Repositories;

use App\Supplier;

class SupplierRepository implements RepositoryInterface
{
    use RepositoryTrait;

    public function __construct(Supplier $model)
    {
        $this->model = $model;
    }

}

